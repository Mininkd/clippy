import tkinter as tk
from tkinter import filedialog
from tkinter import messagebox
from PIL import Image, ImageTk


""" Variables principales """
WORDS_LIST = None   # tous les mots seront enregistrés ici en tant que liste
RESULT_TEMPLATE = "Résultat trouvé à la ligne: "
MAX_VIEW = 10

""" Créer des fonctions qui interagissent avec l'interface """
def upload_file():
    global WORDS_LIST
    # Essayez d'ouvrir un fichier txt valide
    try:
        myfile = filedialog.askopenfilename(
            title = "Sélectionner un fichier",
            filetypes = (
                ("txt files","*.txt"),
                ("all files","*.*")
            )
        )
        # Convertir des données texte en liste python
        WORDS_LIST = list(open(myfile, "r"))
        # Activer le bouton de recherche
        search_button["state"] = "normal"
        search_variable.set("Vous pouvez rechercher maintenant")
    except:
        WORDS_LIST = None
        search_variable.set("Vous n'avez choisi aucun fichier")


def search():
    global FOUND
    # Initialiser le résultat
    FOUND_LINES = ""
    # saisir le mot du formulaire de saisie
    wanted_word = input_word.get()
    for word in WORDS_LIST:
        # sautez "\ n" que python génère au lieu de "retour de ligne".
        searching_word = word.strip("\n")
        # Vérifiez si la recherche se fera avec vérification d'égalité
        if equality_var.get() != 1:
            if searching_word.find(wanted_word) != -1:
                # si le mot trouvé, enregistrez sa ligne pour l'afficher plus tard
                FOUND_LINES += "{}, ".format(WORDS_LIST.index(word) + 1)
        else:
            if searching_word == wanted_word:
                FOUND_LINES += "{}, ".format(WORDS_LIST.index(word) + 1)
        # Afficher le résultat à l'interface
    if FOUND_LINES != "":
        if len(FOUND_LINES.split(", ")) < MAX_VIEW:
            # quand il y a un résultat
            search_variable.set(RESULT_TEMPLATE+FOUND_LINES)
        else:
            search_variable.set("La recherche terminée avec succès")
            messagebox.showinfo("Succès", "le résultat est trop long, vérifiez le fichier 'results.txt'")
            with open("results.txt", "w") as save_result:
                save_result.write(FOUND_LINES.replace(", ", "\n"))
    else:
        # quand il n'y a pas de résultat
        search_variable.set("ce mot est introuvable, essayez-en un autre.")
        

""" Création de l'interface """
# Configuration de la fenêtre principale
root = tk.Tk()
root.title("Clippy") # Modifier le titre de la fenétre
root.resizable(False,False) # Supprimer ce code si vous souhaitez laisser la fenêtre redimensionnable


""" Pour organiser la position des éléments,
nous devons utiliser "Frames", car il est
plus facile de classer leur position """
# Créer Frame
main_frame  = tk.Frame(root, relief="groove")
result_frame  = tk.Frame(root, relief="groove")
watermark_frame  = tk.Frame(root, relief="groove")
# Afficher Frame
main_frame.pack(padx=10, pady=10)
result_frame.pack(side="left", anchor="w", padx=10, pady=10)
watermark_frame.pack(side="right", anchor="e", padx=10, pady=10)
# Bouton de "Sélectionner un fichier"
upload_form =  tk.Button(main_frame,
                text ="Sélectionner un fichier",
                command=upload_file,
            )
upload_form.grid(row=0, column=0, padx=10)
# Étiquette  et formulaire de texte "Chercher"
search_label = tk.Label(main_frame, justify="center", text="Chercher:")
search_label.grid(row=0, column=1)
input_word = tk.Entry(main_frame, justify="center", bd=0, width=20)
input_word.grid(row=0, column=2)
# Bouton de recherche
search_button = tk.Button(main_frame,
                            text ="TROUVER",
                            command=search,
                            font=("", 8, "bold"),
                            state="disabled",
                            )
search_button.grid(row=0, column=3, padx=10)
# Configuration de l'égalité
equality_var = tk.IntVar()
equality_var.set(1)
word_equality =  tk.Checkbutton(main_frame, text="vérifier égalité", variable=equality_var)
word_equality.grid(sticky="w", row=1, column=0, padx=10)
# Le résultat sera affiché ici
search_variable = tk.StringVar()
search_variable.set("Veuillez d'abord sélectionner un fichier.")
search_result = tk.Label(result_frame, justify="left", textvariable=search_variable)
search_result.pack()

# Watermark
load = Image.open("assets/watermark.png")
render = ImageTk.PhotoImage(load)
img = tk.Label(watermark_frame, image=render)
img.image = render
img.pack()
# img.place(x=0, y=0)
# Exécuter l'interface
root.mainloop()