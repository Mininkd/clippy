Le programme posséde la même fonction que notre bon vieil ami clippy présent sur Windows XP ou 
est un dérivé de la bonne fonction CTRL + F des familles.

Dans un premier temps tu choisis un fichier dans lequel tu veux faire ta recherche puis,
Tu tapes le mot que tu souhaite rechercher. 
Par exemple si tu souhaites rechercher le mot "password" le programme va te sortir toutes les 
lignes ou le mot "password" est présent -> password, 123password, strongpassword...

Si tu coches la case vérifier égalité, le programme va vérifier si le mot "password" est présent,
cette fonction est sensible à la casse, donc elle comptera uniquement les lignes contenant le mot 
identique.

S'il y a un trop grand nombre de résultats, un fichier .txt sera créé et les résultats y seront stockés


Made by J-B Vinet et Dit-l'âne Assense

